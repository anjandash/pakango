<?php
include('dbconfig.php');
ini_set('displat_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('UTC');

// SQL QUERIES

function checkUser($email, $password){

	global $con;
	$stmt = $con->prepare("SELECT id FROM users WHERE email=? AND password=?");
	$stmt->bind_param("ss", $email, $password);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;

}

function getUserDataByEmail($email) {

	global $con;
	$stmt = $con->prepare("SELECT * FROM users WHERE email=?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getUserDataByUsername($username) {

	global $con;
	$stmt = $con->prepare("SELECT * FROM users WHERE username=?");
	$stmt->bind_param('s', $username);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function createNewUserData($username, $email, $password){

	global $con;
	$stmt = $con->prepare("INSERT INTO `users` (`username`, `email`, `password`) VALUES (?,?,?)");
	$stmt->bind_param('sss', $username, $email, $password);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function insertIntoNewUsers($fullname, $dob, $username, $email, $password, $countrycode, $phone, $transtype, $businessname, $address, $city, $postalcode, $nation, $partitaiva, $idnum, $rand){

	global $con;
	$stmt = $con->prepare("INSERT INTO `users` (`fullname`, `dob`, `username`, `email`, `password`, `countrycode`, `phone`, `transtype`, `businessname`, `address`, `city`, `postalcode`, `nation`, `partitaiva`, `idnum`, `emailconf`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	$stmt->bind_param('ssssssssssssssss', $fullname, $dob, $username, $email, $password, $countrycode, $phone, $transtype, $businessname, $address, $city, $postalcode, $nation, $partitaiva, $idnum, $rand);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function insertTransporterAds($from_city, $to_city, $date, $from_zone, $to_zone, $pickup_hour, $delivery_hour, $size, $max_dimensions, $max_weight, $contents, $price, $price_small, $price_medium, $price_large, $username, $transtype){

	global $con;
	$stmt = $con->prepare("INSERT INTO `transporter_ads` (`from_city`, `to_city`, `date`, `from_zone`, `to_zone`, `pickup_hour`, `delivery_hour`, `package_size`, `max_dimensions`, `max_weight`, `contents`, `price`, `price_small`, `price_medium`, `price_large`, `transporter`, `transtype`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	$stmt->bind_param('sssssssssssssssss', $from_city, $to_city, $date, $from_zone, $to_zone, $pickup_hour, $delivery_hour, $size, $max_dimensions, $max_weight, $contents, $price, $price_small, $price_medium, $price_large, $username, $transtype);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;	

}

function searchTransporterAds($from_city, $to_city){

	global $con;
	$stmt = $con->prepare("SELECT * FROM transporter_ads WHERE from_city=? AND to_city=?");
	$stmt->bind_param("ss", $from_city, $to_city);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;
}

function searchTransporterAdsByPriv($from_city, $to_city, $str){

	global $con;
	$stmt = $con->prepare("SELECT * FROM transporter_ads WHERE from_city=? AND to_city=? AND transtype=?");
	$stmt->bind_param("sss", $from_city, $to_city, $str);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;
}

function searchTransporterAdsByPro($from_city, $to_city, $str){

	global $con;
	$stmt = $con->prepare("SELECT * FROM transporter_ads WHERE from_city=? AND to_city=? AND transtype=?");
	$stmt->bind_param("sss", $from_city, $to_city, $str);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;
}

function searchTransporterAdsById($id){

	global $con;
	$stmt = $con->prepare("SELECT * FROM transporter_ads WHERE id=?");
	$stmt->bind_param("s", $id);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;
}

function searchTransporterAdsByUser($transporter){

	global $con;
	$stmt = $con->prepare("SELECT * FROM transporter_ads WHERE transporter=? ORDER BY id DESC");
	$stmt->bind_param("s", $transporter);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;
}

function insertMessage($msg_from, $msg_to, $purpid, $msg_body){

	global $con;
	$stmt = $con->prepare("INSERT INTO `messages` (`msg_from`,`msg_to`, `purposeid`,`msg_body`) VALUES (?,?,?,?)");
	$stmt->bind_param("ssss", $msg_from, $msg_to, $purpid, $msg_body);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;		
}

function getMessages($msg_from, $msg_to, $purposeid){

	global $con;
	$stmt = $con->prepare("SELECT * FROM messages WHERE (msg_from, msg_to) IN ((?,?), (?,?)) AND purposeid=?");
	$stmt->bind_param("sssss", $msg_from, $msg_to, $msg_to, $msg_from, $purposeid);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result(); 
	$stmt->close();
	return $result;
}

function msgReadUp($yes, $msg_from){

	global $con;
	$stmt = $con->prepare("UPDATE messages SET msg_read_to=? WHERE msg_to=?");
	$stmt->bind_param("ss", $yes, $msg_from);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) >= 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function chatcheck($msg_from, $msg_to, $purposeid){
	global $con;
	$stmt = $con->prepare("SELECT * FROM chats WHERE (ini_user, rec_user) IN ((?,?), (?,?)) AND purposeid=?");
	$stmt->bind_param("sssss", $msg_from, $msg_to, $msg_to, $msg_from, $purposeid);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function chatcheckByUser($username){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `chats` WHERE ? IN(ini_user, rec_user) ORDER BY id DESC");
	$stmt->bind_param("s", $username);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function insertIntoChats($msg_from, $msg_to, $timestamp, $purposeid, $objectcode){
	global $con;
	$stmt = $con->prepare("INSERT INTO `chats` (`ini_user`,`rec_user`,`time`, `purposeid`, `objectcode`) VALUES (?,?,?,?,?)");
	$stmt->bind_param("sssss", $msg_from, $msg_to, $timestamp, $purposeid, $objectcode);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;

}

function updateChats($msg_from, $msg_to, $timestamp, $purposeid){
	global $con;
	$stmt = $con->prepare("UPDATE chats SET `purposeid`=?, `time`=? WHERE `ini_user`=? AND `rec_user`=?");
	$stmt->bind_param("ssss", $purposeid, $timestamp, $msg_from, $msg_to);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;	
}

function insertIntoObjects($d, $w, $c, $filename, $objectcode){
	global $con;
	$stmt = $con->prepare("INSERT INTO `objects` (`dimensions`, `weights`, `contents`, `filename`, `objectcode`) VALUES (?,?,?,?,?)");
	$stmt->bind_param("sssss", $d, $w, $c, $filename, $objectcode);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function insertSenderRequest($from_city, $to_city, $adid, $sender, $transporter, $getobjectcode, $status){
	global $con;
	$stmt = $con->prepare("INSERT INTO `sender_reqs` (`from_city`, `to_city`, `adid`, `sender`, `transporter`, `objectcode`, `status`) VALUES (?,?,?,?,?,?,?)");
	$stmt->bind_param("sssssss", $from_city, $to_city, $adid, $sender, $transporter, $getobjectcode, $status);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getRequestStatus($adid, $sender){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `sender_reqs` WHERE adid=? AND sender=? ORDER BY id DESC LIMIT 1");
	$stmt->bind_param("ss", $adid, $sender);
	$stmt->execute();

	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function updateRequestStatus($string, $filename, $numbercode, $adid, $sender){
	global $con;
	$stmt = $con->prepare("UPDATE `sender_reqs` SET status=?, qrcode=?, numbercode=? WHERE adid=? AND sender=? ORDER BY id DESC LIMIT 1");
	$stmt->bind_param("sssss", $string, $filename, $numbercode, $adid, $sender);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getActiveBookings($username, $string){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `sender_reqs` WHERE ? IN(`sender`, `transporter`) AND status=? ORDER BY ID DESC");
	$stmt->bind_param("ss", $username, $string);
	$stmt->execute();

	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}


// #############################################################################

function insertNotifs($msg_from, $msg_to, $timestamp, $purposeid, $notiftype){
	global $con;
	$stmt = $con->prepare("INSERT INTO `notifications` (`from_user`, `for_user`, `timestamp`, `purposeid`, `notiftype`) VALUES (?,?,?,?,?)");
	$stmt->bind_param("sssss", $msg_from, $msg_to, $timestamp, $purposeid, $notiftype);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getUserNotifs($username){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `notifications` WHERE for_user=? ORDER BY id DESC");
	$stmt->bind_param("s", $username);
	$stmt->execute();

	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;

}

function getUserNotifsByRead($username, $no){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `notifications` WHERE for_user=? AND notif_read_to=? ORDER BY id DESC");
	$stmt->bind_param("ss", $username, $no);
	$stmt->execute();

	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;

}

function readnotifs($yes, $notifid){
	global $con;
	$stmt = $con->prepare("UPDATE notifications SET notif_read_to=? WHERE id=?");
	$stmt->bind_param("ss", $yes, $notifid);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;

}

// #############################################################################

function getTypeInfoFromNotifs($user1, $user2, $purposeid){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `notifications` WHERE (from_user, for_user) IN ((?,?), (?,?)) AND purposeid=? ORDER BY id DESC LIMIT 1");
	$stmt->bind_param("sssss", $user1, $user2, $user2, $user1, $purposeid);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[mt_rand(0, $max)];
    }
    return $str;
}

function random_num($length, $keyspace = '0123456789')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[mt_rand(0, $max)];
    }
    return $str;
}


function insertIntoCharges($charge_id, $balance_txn, $charge_currency, $charge_amount, $timestamp, $charge_desc, $failure_code, $failure_message, $seller_message, $paid, $receipt_email, $receipt_number, $charge_status, $card_brand, $card_country, $card_exp_month, $card_exp_year, $card_fingerprint, $card_last4, $name_card){

	global $con;
	
	$stmt = $con->prepare("INSERT INTO `charges` (`charge_id`, `balance_txn`, `charge_currency`, `charge_amount`, `timestamp`, `charge_desc`, `failure_code`, `failure_message`, `seller_message`, `paid`, `receipt_email`, `receipt_number`, `charge_status`, `card_brand`, `card_country`, `card_exp_month`, `card_exp_year`, `card_fingerprint`, `card_last4`, `name_card`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


	$stmt->bind_param("ssssssssssssssssssss", $charge_id, $balance_txn, $charge_currency, $charge_amount, $timestamp, $charge_desc, $failure_code, $failure_message, $seller_message, $paid, $receipt_email, $receipt_number, $charge_status, $card_brand, $card_country, $card_exp_month, $card_exp_year, $card_fingerprint, $card_last4, $name_card);

	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;


}

function getPickupPoints($fromcity){
	global $con;
	$stmt = $con->prepare("SELECT * FROM `pickuppoints` WHERE rootcity=? ORDER BY zonelist");
	$stmt->bind_param("s", $fromcity);
	$stmt->execute();

	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

?>