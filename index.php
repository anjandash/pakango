<?php
session_start();
include ('functions.php');

unset($_SESSION['from_city']);
unset($_SESSION['to_city']);

unset($_SESSION['from_zone']);	
unset($_SESSION['to_zone']);
unset($_SESSION['pickup_hour']);
unset($_SESSION['delivery_hour']);

unset($_SESSION['date']);
unset($_SESSION['size']);
unset($_SESSION['price']);

unset($_SESSION['activetype']);
unset($_SESSION['transadactive']);
unset($_SESSION['objects']);

foreach($_SESSION as $key => $val){
    if ($key !== 'email_login'){
      unset($_SESSION[$key]);
    }
}

?>

<head>
	<title>pakango</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="shortcut icon" type="image/png" href="images/pakango-trans.png"> 
	<script type="text/javascript" src="lib/js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript">		
		function slideMenu(){	
			if(document.getElementById('slidemenu').classList.contains('backslidermen') ){
				document.getElementById('slidemenu').classList.remove('backslidermen'); 
				document.getElementById('slidemenu').classList.add('slidermen');
			}			
			document.getElementById('slidemenu').classList.add('slidermen');		
		}
		
		function hide(){
			if(document.getElementById('slidemenu').classList.contains('slidermen') ){
				document.getElementById('slidemenu').classList.remove('slidermen'); 
				document.getElementById('slidemenu').classList.add('backslidermen');
			}
			document.getElementById('slidemenu').classList.add('backslidermen');
		}
	</script>
</head>
<body>
	<div id="wrapper">
		<div id="baseframe">

		<!-- code exp -->

		<div id="slidemenu">
			<div id="cross" onclick="hide();"></div>
			<br/><br/><br/><br/>
			<div id="menutabs" onclick="window.location.href='tframes/profile.php'">PROFILE</div>
			<div id="menutabs" onclick="window.location.href='tframes/chatlist.php'">CHATS</div>
			<div id="menutabs" onclick="window.location.href='tframes/notifications.php'">NOTIFICATIONS</div>
			<div id="menutabs" onclick="window.location.href='tframes/ads.php'">MY ADS</div>
			<div id="menutabs" onclick="window.location.href='tframes/active.php'" style="">ACTIVE BOOKINGS</div>
			<div id="menutabs">TERMS</div>
			<div id="menutabs">PRIVACY</div>
			<div id="menutabs">FAQ</div>
			<div id="menutabs">CONTACT</div>
			<div id="menutabs" onclick="window.location.href='logout.php'">LOGOUT</div>			
		</div>

		<!-- code exp -->

				<div id="sidebar"  style="position: relative; z-index: 1;">
					<div id="menu">
						<div class="leftmenu" onclick="slideMenu();">
							<img src="images/icons/menu.png" id="menuimg">
						</div>
						<div class="titlebox" onclick="window.location.href='index.php'">
							<p>pakango</p>
						</div>
						<div class="leftmenu" onclick="window.location.href='tframes/notifications.php'">
							<img src="images/icons/bell.png" id="menuimg" style="height: 120%;">
						</div>	

						<!-- Notification signal -->
						<?php

							if(isset($_SESSION['email_login'])){

								$email = $_SESSION['email_login'];
								$result = getUserDataByEmail($email);
								$row = mysqli_fetch_assoc($result);
								$username = $row['username'];

								$no = "no";
								$rxquery = getUserNotifsByRead($username, $no);
								$numrows = mysqli_num_rows($rxquery);

								if($numrows > 0){
								?>
								<div style="width: 18px; height: 18px; background: royalblue; color: whitesmoke; font-size: 10px; display: inline-block; vertical-align: top; margin-top: 10px; margin-left: -32px; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%; box-sizing: border-box; padding: 4px 2px; text-align: center;"><?php echo $numrows; ?>
								</div>		
								<?php
								}
 							}

						?>	
						<!-- ##### -->				
				</div>
				
				<!-- base code for the web app-->
				<div id="frame1" style="background: url(images/highway.jpg); background-size: cover; background-repeat: no-repeat; height: 86.5%; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;">

					<div style="height: 100%;">

						<div id="intext" style="margin: 20px 0px 40px;">
							<p style="color: white; font-family: calibri; font-weight: bold;">PACK AND GO!</p>
							<div style="margin: 40px 20px 30px; color: #45b3e0;">
								Start using pakango to safely send or transport packages. Make new friends, and save some money on the way!
							</div>
							<div class="flowcard" onclick="" style="background: transparent; font-weight: normal; color: #45b3e0; border: 1px solid #45b3e0;">
								COME FUNZIONA
							</div>

						</div>

						<div class="flowcard" style="background: transparent; color: #FF8C0F; border: 1px solid #FF8C0F;" onclick="window.location.href='tframes/destination-snd.php?flow=sender'">
							SPEDISCI
						</div>

						<div class="flowcard" style="background: #FF8C0F; color: whitesmoke;" onclick="window.location.href='tframes/destination.php?flow=transporter'">
							TRASPORTA
						</div>		



						<div class="titlebox" style="border: 1px solid transparent; margin: 40px auto 0px;">	
							<a href="tframes/login.php" style="text-decoration: none; color: #FF8C0F; font-family: calibri; font-style: italic; font-weight: normal;">My Account</a>					
						</div>
					</div>

				</div>
				<!-- base code for the web app-->

			</div>
		</div>
	</div>
	<!-- <div id="footframe">			
	</div> -->
</body>

