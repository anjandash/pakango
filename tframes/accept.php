<?php
session_start();
include ('../functions.php');

if(isset($_GET['adid']) && isset($_GET['sender'])){
	$adid = $_GET['adid'];
	$sender = $_GET['sender'];
}

/* QR CODE BLOCK */

include('../phpqrcode/qrlib.php');

$qquery = searchTransporterAdsById($adid);
$qrow = mysqli_fetch_assoc($qquery);
$qdate = new DateTime();
$qtimestamp = $qdate->getTimestamp();

$tempDir = "../qrcodes/";
$codeContents = $qrow['from_city']." ".$qrow['to_city']."\n".$qrow['date']." By: ".$qrow['transporter']."\n Sender: @".$sender;
$fileName = 'qrcode-'.$adid.'-'.$sender.'-'.$qtimestamp.'.png';

$pngAbsoluteFilePath = $tempDir.$fileName;
$urlRelativeFilePath = "../qrcodes/".$fileName;

QRcode::png($codeContents, $pngAbsoluteFilePath);


/* QR CODE BLOCK */

$numbercode = random_num(6);

$string = "accepted";
$queryrev = updateRequestStatus($string, $fileName, $numbercode, $adid, $sender);

if($queryrev){

	$date = new DateTime();
	$timestamp = $date->getTimestamp();

	$active_email_rev = $_SESSION['email_login'];
	$resxrev = getUserDataByEmail($active_email_rev);
	$rowxrev = mysqli_fetch_assoc($resxrev);
	$transporter = $rowxrev['username'];

	$notiftype = "booked";
	$notifquery = insertNotifs($transporter, $sender, $timestamp, $adid, $notiftype);
	if($notifquery){}else{echo "<script>console.log('Error: Insert into Notifs failed! (booked)');</script>";}	


    if (isset($_SERVER["HTTP_REFERER"])) {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }

} else {
	echo "Oops! Something went wrong";
}

?>