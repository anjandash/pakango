<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION['email_login'])){
	header('location: login.php?m=loginfirst');
	exit();
}

$active_email = $_SESSION['email_login'];
$resx = getUserDataByEmail($active_email);
$rowx = mysqli_fetch_assoc($resx);
$username = $rowx['username'];

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%; margin-top: 10px;">

					<div id="notifbar">
						<?php
							$string = "accepted";

							$query = getActiveBookings($username, $string);

							if(mysqli_num_rows($query) >= 1){} else { 
								echo "<div style='margin: 40px 0px; font-size: 14px; color: grey;'>You do not have any active bookings at the moment!</div>"; 
							}

							while($row = mysqli_fetch_assoc($query)){									
							?>					

						<div class="actbx" onclick="window.location.href='status.php?adid=<?php echo $row['adid']; ?>&senderx=<?php echo $row['sender']; ?>'">
							<p style="color: grey; font-style: normal;"><b>CONFIRMED BOOKING</b></p>
							<div class="">
								<?php				

										echo "<div style='text-align:left; margin: 10px auto; padding-left: 10px;'>";
										echo "From: ".$row['from_city']."&nbsp;";
										echo "To: ".$row['to_city']."<br/><br/>";
										echo "Sender: ".$row['sender']."<br/>";
										echo "Transporter: ".$row['transporter']."<br/>";
										echo "STATUS: <span style='color:green;'>".$row['status']."</span><br/>";
										echo "</div>";								
								?>								
							</div>
						</div>

						<?php
							}
						?>
					</div>


					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


