<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION["email_login"])){
	header('location: login.php');
	exit();
}

$email = $_SESSION['email_login'];
$result = getUserDataByEmail($email);
$row = mysqli_fetch_assoc($result);
$transporter = $row['username'];
?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%; margin-top: 10px;">

					<div id="notifbar">						

					<?php
						$que = searchTransporterAdsByUser($transporter);

						if(mysqli_num_rows($que) >= 1){} else { 
							echo "<div style='margin: 40px 0px; font-size: 14px; color: grey;'>You do not have any active ads yet!</div>"; 
						}

						while ($res = mysqli_fetch_assoc($que)) { 
						?>

						<div class="notif" onclick="">
							<div class="notifdetails" style="width: 90%; padding-top: 20px;">
								<p>Ad published for <b><?php echo $res['from_city']."-".$res['to_city']; ?></b></p>
								<p>Date<span style="font-family: arial;">/</span>Time: <?php echo $res['date']."&nbsp;at&nbsp;".$res['pickup_hour']; ?> hrs<br/>By <span style="color: royalblue; font-weight: bolder;"><?php echo $res['transporter']; ?></span></p>
							</div>
							<div></div>
						</div>	

						<?php						
						}

					?>



					</div>


					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


