<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION['email_login'])){
	header('location: login.php?m=loginfirst');
	exit();
}

if(isset($_GET['to'])){
	$msg_to = $_GET['to'];

	$from_email = $_SESSION['email_login'];
	$resx = getUserDataByEmail($from_email);
	$rowx = mysqli_fetch_assoc($resx);
	$msg_from = $rowx['username'];
}

if(isset($_POST['msgsend'])){
	$msg_body = $_POST['msg'];
	$msg_body = trim($msg_body);

	// Add purposeid here................... 
	if(isset($_GET['purposeid'])){
		$purpid = $_GET['purposeid'];
	}

	if($msg_body){

		$resv = insertMessage($msg_from, $msg_to, $purpid, $msg_body);
		if($resv){}else{ echo "Error: Sorry! message could not be sent!"; }
	}
}	

// ##############################

if($msg_to == $msg_from){
	header('location: profile.php?m=sameuser');
}

if(isset($_GET['notifread'])){
	$notifid = $_GET['notifread'];

	$yess = "yes";
	$readnotq = readnotifs($yess, $notifid);
	if($readnotq){}else{ echo "<script>console.log('Notification Update failed!');</script>"; }
}


// ################################### 

if(isset($_SESSION['transadactive'])){
	$purposeid = $_SESSION['transadactive'];
	unset($_SESSION['transadactive']);


	// ----------------------------------------------------------------------------------
		function incrementalHash($len){
		  $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		  $base = strlen($charset);
		  $result = '';

		  $now = explode(' ', microtime())[1];
		  while ($now >= $base){
		    $i = $now % $base;
		    $result = $charset[$i] . $result;
		    $now /= $base;
		  }
		  return substr($result, -5);
		}
	// -----------------------------------------------------------------------------------

	$objectcode = incrementalHash(8);

	$chatquery = chatcheck($msg_from, $msg_to, $purposeid);
	$rowcount = mysqli_num_rows($chatquery);
	if($rowcount == 0){			
		$date = new DateTime();
		$timestamp = $date->getTimestamp();

		$insec = insertIntoChats($msg_from, $msg_to, $timestamp, $purposeid, $objectcode);

		if($insec){
			$notiftype = "chat";
			$notifquery = insertNotifs($msg_from, $msg_to, $timestamp, $purposeid, $notiftype);
			if($notifquery){echo "<script>console.log('Notif pushed!');</script>";}else{echo "<script>console.log('Error: Insert into Notifs failed! (chat)');</script>";}
		}else{ 
			echo "Oops! Error!"; 
		}


		// insert objects into objects table && insert objectcode 
		$numb = $_SESSION['objects'];
		for($k = 1; $k <= $numb; $k++){

			$d = $_SESSION['dimensions'.$k];
			$w = $_SESSION['weights'.$k];
			$c = $_SESSION['contents'.$k];

			if(isset($_SESSION['photos'.$k])){
				$filename = $_SESSION['photos'.$k];
			} else{
				$filename = "";
			}
			
			$objectquery = insertIntoObjects($d, $w, $c, $filename, $objectcode);

			if($objectquery){
				echo "<script>console.log('Object code:".$objectcode." - Object number:".$k." - Inserted!');</script>";
			}else{
				echo "<script>console.log('Object code:".$objectcode." - Object number:".$k." - ERROR on Insert.');</script>";
			}			
		}

	}
	
}


// ################################### 

if(isset($_POST['booksub'])){

	$from_city = strip_tags(@$_POST['from_city']);
	$to_city = strip_tags(@$_POST['to_city']);
	$adid = strip_tags(@$_POST['adid']);
	$sender = strip_tags(@$_POST['sender']);
	$transporter = strip_tags(@$_POST['transporter']);
	$status = strip_tags(@$_POST['status']);
	$getobjectcode = strip_tags(@$_POST['xobjectcode']);

	$insquery = insertSenderRequest($from_city, $to_city, $adid, $sender, $transporter, $getobjectcode, $status);


	/* insert the objects here by using the id from sender_reqs */


	if($insquery){

		$date = new DateTime();
		$timestamp = $date->getTimestamp();

		$notiftype = "booking";
		$notifqueryx = insertNotifs($sender, $transporter, $timestamp, $adid, $notiftype);
		if($notifqueryx){}else{echo "<script>console.log('Error: Insert into Notifs failed! (booking)');</script>";}


		header('location: status.php?adid='.$adid);
	}else{ 
		echo "Error: Sorry! your booking could not be processed!"; 
	}

}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 90%; margin-top: -30px;">


					<?php if(true){ 
						if(isset($_GET['purposeid'])){
							$purposeid = $_GET['purposeid'];
						}

						$res = searchTransporterAdsById($purposeid);
						$row = mysqli_fetch_assoc($res);

						// ------------------------------------------

							$xquery = chatcheck($msg_from, $msg_to, $purposeid);
							$xrow = mysqli_fetch_assoc($xquery);

							if($xrow == 0){} else { $xobjectcode = $xrow['objectcode']; }

						// ------------------------------------------
					?>
					<div id="dispcard" style="height: auto; font-size: 12px; text-align: left; padding: 10px 20px; box-sizing: border-box;">
						<p><b>From:	<?php echo $row['from_city']." (".$row['from_zone'].") <br/>To: ".$row['to_city']." (".$row['to_zone'].")"; ?></b></p>
						<p>Date: <?php echo $row['date']; ?><br/>
						<p>Departure hour: <?php echo $row['pickup_hour']; ?></p>
						<p>Arrival hour: <?php echo $row['delivery_hour']; ?></p>
						<p id="prc">Price: From €<?php echo $row['price_small']; ?></p>
						<p id="prcx" style="margin-top: -47px;"><?php echo $row['transtype']; ?></p>
						By <span style="color: royalblue; font-weight: bolder;"><?php echo $row['transporter']; ?></span></p>

						<form id="book-form" action="" method="POST">
							<input type="hidden" name="from_city" value="<?php echo $row['from_city']; ?>">
							<input type="hidden" name="to_city" value="<?php echo $row['to_city']; ?>">
							<input type="hidden" name="adid" value="<?php echo $row['id']; ?>">
							<input type="hidden" name="sender" value="<?php echo $msg_from; ?>">
							<input type="hidden" name="transporter" value="<?php echo $row['transporter']; ?>">
							<input type="hidden" name="status" value="requested">
							<input type="hidden" name="xobjectcode" value="<?php echo $xobjectcode; ?>">
							<input type="submit" name="booksub" id="booksubbtn" style="display: none;">
						</form>

						<?php 

						if ($msg_to == $row['transporter']){
							$findquery = getRequestStatus($row['id'], $msg_from);
							$rowz = mysqli_fetch_assoc($findquery);		
						} else {
							$findquery = getRequestStatus($row['id'], $msg_to);
							$rowz = mysqli_fetch_assoc($findquery);								
						}

						if($rowz['status'] == 'requested'){

							if ($msg_to == $row['transporter']){
							?><div class="bookbtn requested" onclick="window.location.href='status.php?adid=<?php echo $row['id']; ?>'">REQUESTED</div><?php
							} else {
							?><div class="bookbtn accept" onclick="window.location.href='accept.php?adid=<?php echo $row['id']; ?>&sender=<?php echo $msg_to; ?>'">ACCEPT</div><?php
							}

						} elseif ($rowz['status'] == 'accepted') {

							if ($msg_to == $row['transporter']){
							?><div class="bookbtn accepted" onclick="window.location.href='status.php?adid=<?php echo $row['id']; ?>'">BOOKED</div><?php
							} else {
							?><div class="bookbtn accepted" onclick="window.location.href='status.php?adid=<?php echo $row['id']; ?>&senderx=<?php echo $msg_to; ?>'">BOOKED</div><?php
							}


						} elseif ($rowz['status'] == 'declined') {

							if ($msg_to == $row['transporter']){
							?><div class="bookbtn declined" onclick="window.location.href='status.php?adid=<?php echo $row['id']; ?>'">DECLINED</div><?php
							} else {
							?><div class="bookbtn declined" onclick="window.location.href='status.php?adid=<?php echo $row['id']; ?>&senderx=<?php echo $msg_to; ?>'">DECLINED</div><?php								
							}

						} else {

							if ($msg_to == $row['transporter']){

								if($row['transtype'] == 'private'){
								?>
									<div class="bookbtn" onclick="document.getElementById('booksubbtn').click();">BOOK</div>
								<?php
								}else{
								?>
									<div class="bookbtn" onclick="window.location.href='payment/payx1.php?purposeid=<?php echo $row['id']; ?>'">BOOK</div>
								<?php
								}

							} else {
								?>
								<div class="bookbtn requested" onclick="">UNBOOKED</div>
								<?php								
							}
						}


						if(isset($_GET['book'])){
							$bookact = $_GET['book'];

							if($bookact == "active"){
								echo"<script>
										document.getElementById('booksubbtn').click();
									</script>";
							}
						}

						?>						
						
						
					</div>
					<?php }else{ ?>
					<div id="dispcard" style="height: auto; font-size: 12px; padding: 10px 20px; box-sizing: border-box;">
						<?php echo $msg_to; ?>
					</div>
					<?php } ?>

					<div id="notifbar" style="position: relative;">
						<?php
						$line = true;
						$resn = getMessages($msg_from, $msg_to, $purposeid);
						while($rown = mysqli_fetch_assoc($resn)){

							if($rown['msg_from'] != $msg_from){
								if($rown['msg_read_to'] == "no"){
									if($line){
										echo "<div id='unline' style='border-bottom: 1px solid lightgrey; float:left; width: 100%; margin: 10px 0px; height:auto;'>unread messages</div>";
										$line = false;
									}

								}
							}

							if($rown['msg_from'] == $msg_from){
							?><div style="border: 1px solid orangered; text-align: left; padding: 5px 10px; width: 60%; height: auto; margin: 10px 5px; float: right;"><?php echo $rown['msg_body']; ?></div><br/><?php
							}else{
							?><div style="border: 1px solid lightgrey; text-align: left; padding: 5px 10px; width: 60%; height: auto; margin: 10px 5px; float: left;"><?php echo $rown['msg_body']; ?></div><?php
							}
						}
						?>
					</div>

					<div id="sender">
						<form action="" method="POST" autocomplete="off">
						<div id="inparea">
							<input type="text" name="msg" style="height: 100%; width: 100%; border-bottom: 1px solid whitesmoke; padding: 0px 5px; margin-top: 0px;" autocomplete="off" required>
						</div>
						<div style="display: inline-block; width: 20%;">
							<!-- ##### SEAMLESS SENDING OF MESSAGES = ONCLICK CALL AJAX FUNCTION = W/OUT PAGE REFRESH ON EVERY SUBMIT --> 
							<input type="submit" name="msgsend" style="width: 100%; height: 100%; border-radius: 0px; margin: 0px;" value="send">
						</div>
						</form>
					</div>


					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->



			<?php
				if($rown['msg_from'] != $msg_from){
					$yes = "yes";
					$query = msgReadUp($yes, $msg_from);
					if($query){ }else{ echo "<script>console.log( 'Internal Error: Sorry! msgReadUp query not updated!' );</script>"; }
				}
			?>

			<script type="text/javascript">
				(function (){	
						var x = $('#unline').position();
						document.getElementById('notifbar').scrollTop = document.getElementById('notifbar').scrollHeight;
						document.getElementById('notifbar').scrollTop = x.top - 20;		
				})();		
			</script>

<?php include 'commons/footer.php'; ?>		

