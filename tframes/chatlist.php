<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION["email_login"])){
	header('location: login.php');
	exit();
}

$email = $_SESSION['email_login'];
$result = getUserDataByEmail($email);
$row = mysqli_fetch_assoc($result);
$username = $row['username'];
?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%; margin-top: 10px;">

					<div id="notifbar">						

					<?php
						$que = chatcheckByUser($username);

						if(mysqli_num_rows($que) >= 1){} else { 
							echo "<div style='margin: 40px 0px; font-size: 14px; color: grey;'>You do not have any active chats yet!</div>"; 
						}


						while ($res = mysqli_fetch_assoc($que)) { 
							if($res['ini_user'] == $username){ $chatto =  $res['rec_user'];} else { $chatto = $res['ini_user'];} 
						?>

						<div class="notif" style="height: 60px; margin: 8px auto;" onclick="window.location.href='chat.php?to=<?php echo $chatto; ?>&purposeid=<?php echo $res['purposeid']; ?>'">
							<div class="notifdetails" style="line-height: 43px; padding-left: 10px;">
								<div id="chatdp"></div>
								<div style="display: inline-block; vertical-align: top; overflow-x: clip;"><span style="color: orange; font-size: 18px;"><?php echo $chatto; ?></span></div>
							</div>

							<div class="chatarea" style="background: white; border: 1px solid orange; font-size: 10px; height: auto; margin-top: 6px;">	
								<?php
									$typequery = getTypeInfoFromNotifs($res['ini_user'], $res['rec_user'], $res['purposeid']);
									$typeres = mysqli_fetch_assoc($typequery);
								?>
								<p style="color: orange;"><?php echo $typeres['notiftype']; ?></p>						
							</div>

						</div>	

						<?php						
						}

					?>



					</div>


					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


