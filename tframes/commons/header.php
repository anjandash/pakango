<head>
	<title>pakango</title>
	<link rel="stylesheet" type="text/css" href="../lib/css/style.css">
	<link rel="shortcut icon" type="image/png" href="../images/pakango-trans.png"> 
	<script type="text/javascript" src="../lib/js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript">		
		function slideMenu(){	
			if(document.getElementById('slidemenu').classList.contains('backslidermen') ){
				document.getElementById('slidemenu').classList.remove('backslidermen'); 
				document.getElementById('slidemenu').classList.add('slidermen');
			}			
			document.getElementById('slidemenu').classList.add('slidermen');		
		}
		
		function hide(){
			if(document.getElementById('slidemenu').classList.contains('slidermen') ){
				document.getElementById('slidemenu').classList.remove('slidermen'); 
				document.getElementById('slidemenu').classList.add('backslidermen');
			}
			document.getElementById('slidemenu').classList.add('backslidermen');
		}
	</script>
</head>
<body>
	<div id="wrapper">

		<div class="messenger">
		</div>

		<div id="baseframe">

		<!-- code exp -->

		<div id="slidemenu">
			<div id="cross" onclick="hide();"></div>
			<br/><br/><br/><br/>
			<div id="menutabs" onclick="window.location.href='profile.php'">PROFILE</div>
			<div id="menutabs" onclick="window.location.href='chatlist.php'">CHATS</div>
			<div id="menutabs" onclick="window.location.href='ads.php'">MY ADS</div>
			<div id="menutabs" onclick="window.location.href='active.php'"> BOOKINGS</div>
			<div id="menutabs">TERMS</div>
			<div id="menutabs">PRIVACY</div>
			<div id="menutabs">FAQ</div>
			<div id="menutabs">CONTACT</div>
			<div id="menutabs" onclick="window.location.href='../logout.php'">LOGOUT</div>			
		</div>

		<!-- code exp -->

			<div id="sidebar"  style="position: relative; z-index: 1;">
					<div id="menu">
						<div class="leftmenu" onclick="slideMenu();">
							<img src="../images/icons/menu.png" id="menuimg">
						</div>
						<div class="titlebox" onclick="window.location.href='../index.php'">
							<p>pakango</p>
						</div>
						<div class="leftmenu" onclick="window.location.href='notifications.php'">
							<img src="../images/icons/bell.png" id="menuimg" style="height: 120%;">
						</div>	

						<!-- Notification signal -->

						<?php
							
							if(isset($_SESSION['email_login'])){

								$emailmade = $_SESSION['email_login'];
								$resultmade = getUserDataByEmail($emailmade);
								$rowmade = mysqli_fetch_assoc($resultmade);
								$usernamemade = $rowmade['username'];

								$no = "no";
								$rxquery = getUserNotifsByRead($usernamemade, $no);
								$numrows = mysqli_num_rows($rxquery);

								if($numrows > 0){
								?>
								<div style="width: 18px; height: 18px; background: royalblue; color: whitesmoke; font-size: 10px; display: inline-block; vertical-align: top; margin-top: 10px; margin-left: -32px; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%; box-sizing: border-box; padding: 4px 2px; text-align: center;"><?php echo $numrows; ?>
								</div>		
								<?php
								}
 							}

						?>	
						<!-- ##### -->				
					</div>