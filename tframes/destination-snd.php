<?php
session_start();
include '../functions.php';

$destinationsub="";

if(isset($_POST['destinationsub'])){
	$destinationsub = @$_POST['destinationsub'];
}

if($destinationsub){
	$_SESSION['from_city'] = @$_POST['from'];
	$_SESSION['to_city'] = @$_POST['to'];
	$_SESSION['date'] = @$_POST['date'];
	$_SESSION['pickup_hour'] = @$_POST['pickuphr'];
	$_SESSION['delivery_hour'] = @$_POST['deliveryhr'];	
	$_SESSION['objects'] = @$_POST['objects'];

	header("location: objects.php");
	exit();
}

if(isset($_GET['flow'])){
	$flow = $_GET['flow'];

	if($flow == "transporter"){
		$_SESSION['activetype'] = "transporter";
		header("location: destination.php");
		exit();
	}elseif ($flow == "sender") {
		$_SESSION['activetype'] = "sender";
		header("location: destination-snd.php");
		exit();
	}
}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 75%; margin-top: 30px;">
					
					<div style="height: 100%; overflow-y: scroll;">
					<form id="destination-form" action="" method="POST">
					
						<p id="label">Partenza da:</p>
						<select name="from" required>
						  <option value="Bolzano">Bolzano</option>
						  <option value="Trento">Trento</option>
						</select><br/><br/>

						<p id="label">Destinazione:</p>
						<select name="to" required>
						  <option value="Bolzano">Bolzano</option>
						  <option value="Trento">Trento</option>
						</select><br/><br/>

						<p id="label">Date:</p>
						<input type="date" name="date" data-date-format="DD MM YYYY" required><br/><br/>

						<!-- <p id="label">Orario di partenza:</p>
						<input type="text" name="pickuphr" placeholder="Pickup Hour:" required><br/><br/>
						<p id="label">Orario di arrivo previsto:</p>
						<input type="text" name="deliveryhr" placeholder="Delivery Hour:" required><br/><br/> -->
						<p id="label">No. of Objects:</p>	
						<input type="number" name="objects" placeholder="No of objects:" required><br/><br/>

						<input type="submit" name="destinationsub" value="CONTINUE"><br/>					
					</form>
					</div>

				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>				




