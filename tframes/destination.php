<?php
session_start();
include('../functions.php');
$destinationsub="";

if(isset($_POST['destinationsub'])){
	$destinationsub = @$_POST['destinationsub'];
}

if($destinationsub){
	$_SESSION['from_city'] = @$_POST['from'];
	$_SESSION['to_city'] = @$_POST['to'];
	$_SESSION['date'] = @$_POST['date'];

	$phr = (@$_POST['pickuphr'])." : ".(@$_POST['pickuphr2']);
	$dhr = (@$_POST['deliveryhr'])." : ".(@$_POST['deliveryhr2']);

	$_SESSION['pickup_hour'] = $phr;
	$_SESSION['delivery_hour'] = $dhr;	

	header("location: zone.php");
	exit();
}

if(isset($_GET['flow'])){
	$flow = $_GET['flow'];

	if($flow == "transporter"){
		$_SESSION['activetype'] = "transporter";
		header("location: destination.php");
		exit();
	}elseif ($flow == "sender") {
		$_SESSION['activetype'] = "sender";
		header("location: destination.php");
		exit();
	}
}
?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%;">
					
					<div style="height: 100%; overflow-y: scroll;">
					<form id="destination-form" action="" method="POST">
					
						<p id="label">Partenza da:</p>
						<select name="from" required>
						  <option value="">From City</option>
						  <option value="Bolzano">Bolzano</option>
						  <option value="Trento">Trento</option>
						</select><br/><br/>

						<p id="label">Destinazione:</p>
						<select name="to" required>
						  <option value="">To City</option>
						  <option value="Bolzano">Bolzano</option>
						  <option value="Trento">Trento</option>
						</select><br/><br/>

						<p id="label">Date:</p>
						<input type="date" name="date" data-date-format="DD MM YYYY" required><br/><br/>

						<p id="label">Orario di partenza:</p>
						<select name="pickuphr" style="width: 120px;" required>
						  <option value="">Hour</option>
						  <option value="00">00</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						  <option value="13">13</option>
						  <option value="14">14</option>
						  <option value="15">15</option>
						  <option value="16">16</option>
						  <option value="17">17</option>
						  <option value="18">18</option>
						  <option value="19">19</option>
						  <option value="20">20</option>
						  <option value="21">21</option>
						  <option value="22">22</option>
						  <option value="23">23</option>						  						  
						</select>
						&nbsp;&nbsp;&nbsp;
						<select name="pickuphr2" style="width: 120px;" required>
						  <option value="">Minute</option>
						  <option value="00">00</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>

						  <?php	for($i = 11; $i<60; $i++){ ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?>
						</select><br/><br/>

						<p id="label">Orario di arrivo previsto:</p>
						<select name="deliveryhr" style="width: 120px;" required>
						  <option value="">Hour</option>
						  <option value="00">00</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						  <option value="13">13</option>
						  <option value="14">14</option>
						  <option value="15">15</option>
						  <option value="16">16</option>
						  <option value="17">17</option>
						  <option value="18">18</option>
						  <option value="19">19</option>
						  <option value="20">20</option>
						  <option value="21">21</option>
						  <option value="22">22</option>
						  <option value="23">23</option>						  						  
						</select>
						&nbsp;&nbsp;&nbsp;
						<select name="deliveryhr2" style="width: 120px;" required>
					      <option value="">Minute</option>
						  <option value="00">00</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>

						  <?php	for($i = 11; $i<60; $i++){ ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?>
						</select><br/><br/>	

						<input type="submit" name="destinationsub" value="CONTINUE"><br/>
						<!-- <div class="backbutton" onclick="window.location.href='../index.php'">
							<p>BACK</p>
						</div> -->						
					</form>
					</div>

				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>				




