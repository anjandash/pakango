<?php
session_start();
include('../functions.php');
$dimsub="";

if(isset($_SESSION['activetype'])){
	if($_SESSION['activetype'] == "transporter"){
		$next = 'price.php';
	}elseif ($_SESSION['activetype'] == "sender") {
		$next = 'pricing.php';
	}else {
		$next = 'index.php?m=error';
	}
}

if(isset($_POST['dimsub'])){
	$dimsub = @$_POST['dimsub'];
}

if($dimsub){
	$_SESSION['size'] = "dummvalue";

	// new adds
	$_SESSION['max_dimensions'] = @$_POST['maxdim'];
	$_SESSION['max_weight'] = @$_POST['maxweight'];
	$_SESSION['contents'] = @$_POST['contents'];

	header("location: ".$next);
	exit();
}

?>

<?php include 'commons/header.php'; ?>

				<script type="text/javascript">
					// function selector(ele, size){
					// 	var elements = document.getElementsByClassName('dimbox');
					//     for(var i = 0, length = elements.length; i < length; i++) {
					//        if( elements[i].textContent != ''){
					//           elements[i].style.background = '#FE7C4C';
					//        } 
					//     }
					//     ele.style.background = '#E95A62';
					//     document.getElementById('inpsize').value = size;
					// }
				</script>

				<!-- base code for the web app-->
				<div id="frame1" style="height: 90%;">

					<form id="dimensions-form" action="" method="POST">
					
						<!-- <div class="dimbox" onclick="selector(this, 'small')">small</div>
						<div class="dimbox" onclick="selector(this, 'medium')">medium</div>
						<div class="dimbox" onclick="selector(this, 'big')">big</div>
						<div class="dimbox" onclick="selector(this, 'large')">large</div> -->

						<br/><br/>
						<!-- <input type="text" name="dimmax" placeholder="max dimensions"> -->

						<p id="label">Max. Dimensions:</p>
						<select name="maxdim" required>
						  <option value="30cm x 30cm">30cm x 30cm (small)</option>
						  <option value="60cm x 60cm">60cm x 60cm (medium)</option>
						  <option value="120cm x 120cm">120cm x 120cm (large)</option>
						</select><br/><br/>


						<!-- <input type="text" name="weightmax" placeholder="max weight"> -->

						<p id="label">Max. Weight:</p>
						<select name="maxweight" required>
						  <option value="Upto 5kg">Upto 5 kg</option>
						  <option value="Upto 8kg">Upto 8kg</option>
						  <option value="Upto 12kg">Upto 12kg</option>
						</select><br/><br/>

						<p id="label">Contents &amp; Comments:</p>
						<input type="text" name="contents" placeholder="Fragile item, Books, Personal, etc.">


						<!-- <div style="width: 22%; text-align: left; margin: -20px auto; visibility: hidden;">
							<input type="text" name="size" id="inpsize" value="" required><br/>
						</div>	 -->


						<input type="submit" name="dimsub" value="CONTINUE"><br/>
						<!-- <div class="backbutton" onclick="window.location.href='zone.php'">
							<p>BACK</p>
						</div> -->
					</form>

					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>				



