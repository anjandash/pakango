<?php
session_start();
include ('../functions.php');

$text = "";
$from_city =  $_SESSION['from_city'];
$to_city =  $_SESSION['to_city'];



if(isset($_GET['filter'])){
	$text = $_GET['filter'];
}

if($text == "all"){
	$res = searchTransporterAds($from_city, $to_city);
} elseif ($text == "priv") {
	$str = "private";
	$res = searchTransporterAdsByPriv($from_city, $to_city, $str);
} elseif ($text == "pro") {
	$str = "professional";
	$res = searchTransporterAdsByPro($from_city, $to_city, $str);
} else {
	$res ="";
}

$i = 0;
$arr = [[]];



while($x = mysqli_fetch_assoc($res)){

	foreach($x as $key => $value){
			$arr[$i][$key] = $value;	
	}

	$i++;
}

echo (json_encode($arr)); 

?>