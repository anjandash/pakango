<?php
session_start();
include ('../functions.php');

if(isset($_GET['log'])){
	$log = $_GET['log'];

	if($log == "failedlogin"){
		echo "<div class='messenger' style='opacity: 1; padding: 5px; top:55px;'>Oops! Login failed!</div>";

		/* put message for wrong password */
	}

	if($log == "noaccount"){
		echo "<div class='messenger' style='opacity: 1; padding: 5px; top:55px;'>Oops! There is no account with that email!</div>";
	}	
}

if(isset($_GET['m'])){
	$m = $_GET['m'];

	if($m == "loginfirst"){
		echo "<div class='messenger' style='opacity: 1; padding: 5px; top:55px;'>Please login get access!</div>";
	}
}

//LOGIN BLOCK

if(!isset($_SESSION["email_login"])){
	if (isset($_POST["email_login"]) && isset($_POST["password_login"])) {

		$email_login = filter_var($_POST["email_login"], FILTER_SANITIZE_STRING);

		$password_login = $_POST["password_login"];
		$password_login_sha = openssl_digest($password_login, 'sha512');

		$sql = checkUser($email_login, $password_login_sha);
		$userCount = mysqli_num_rows($sql);

		// echo "<script>alert('".$userCount."');</script>";

		if($userCount > 0) {
			while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
				$id = $row["id"];
			}
			
			$_SESSION["email_login"] = $email_login;	
			?><script>window.location.href='profile.php?log=success';</script><?php

		} elseif ($userCount == 0) {
			?><script>window.location.href='login.php?log=noaccount'; </script><?php
		}
		else{		
			?><script>window.location.href='login.php?log=failedlogin'; </script><?php
		}		
	}
}
else{
	// if SESSION LOGIN is active - Forward to PROFILE.PHP page
	header('location: profile.php');
	exit();
}


?>

<?php include 'commons/header.php'; ?>

				<div style="width: 350px; height: 150px; background: linear-gradient(90deg, #FE801C 0%, #FE5A43 100%);
				border-radius: 150px / 75px; margin-top: -70px; margin-left: 0px; position: relative; z-index: -2; overflow-x: visible; overflow-y: hidden;">
				</div>

				<div id="loginlogo">
				</div>
				
				<!-- base code for the web app-->
				<div id="frame1" style="padding-top: 0px; height: 65%;">

					<form id="login-form" action="login.php" method="POST" autocomplete="off">
						<div style="margin: 5px auto;"></div>
							<!-- -->
							<input type="hidden" name="email" placeholder="Email" autocomplete="off">
							<input type="hidden" name="password" placeholder="Password" autocomplete="off">
							<!-- -->							
							<input type="email" name="email_login" placeholder="Email" autocomplete="new-password" required>
							<input type="password" name="password_login" placeholder="Password" autocomplete="new-password" required>
							<hr style="border: 1px solid transparent;">
							<input type="submit" id="logsub" name="login" value="login"><br/>
							
					</form>
					
					<div id="sociallog">
						<div style="height: 10px; width: 90%; border-bottom: 1px solid lightgrey; margin: 20px auto 10px;">
							<p style="width: 140px; background: white; margin: 0px auto; color: lightgrey;">or register with</p>
						</div>	

						<hr style="border: 1px solid transparent;">
						
						<div class="titlebox" style="width: 80%; border: 1px solid black; color: grey;" onclick="window.location.href='register.php'">email</div>				
						<div class="titlebox" style="width: 80%; border: 1px solid blue; color: blue;">facebook</div>	
						<div class="titlebox" style="width: 80%; border: 1px solid red; color: red;">google</div>
					</div>
					
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


