<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION['email_login'])){
	header('location: login.php?m=loginfirst');
	exit();
}

$active_email = $_SESSION['email_login'];
$resx = getUserDataByEmail($active_email);
$rowx = mysqli_fetch_assoc($resx);
$username = $rowx['username'];

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%; margin-top: 10px;">

					<div id="notifbar">
						<?php
							$notifquery = getUserNotifs($username);

							if(mysqli_num_rows($notifquery) >= 1){} else { 
								echo "<div style='margin: 40px 0px; font-size: 14px; color: grey;'>You do not have any notifications at the moment!</div>"; 
							}

							while($row = mysqli_fetch_assoc($notifquery)){
									$address = "chat.php?notifread=".$row['id']."&to=".$row['from_user']."&purposeid=".$row['purposeid'];

							?>
							<div class="notif" onclick="window.location.href='<?php echo $address; ?>'">
								<div class="notifdetails">
									<p style="margin-bottom: 5px;">
									You have a new <b><?php echo $row['notiftype']." request from @".$row['from_user']."!<br/>"; ?></b>
									</p>
									<?php
										$squery = searchTransporterAdsById($row['purposeid']);
										$srow = mysqli_fetch_assoc($squery);

										echo "Route: ".$srow['from_city']." - ".$srow['to_city']."<br/>";
										echo "Date: ".$srow['date'];
									?>
								</div>

								<?php 
									if($row['notif_read_to'] == "no"){
										?><div id="dot"></div><?php
									} else {
										?><div id="dot" style="background: white;"></div><?php
									}
								?>						
							</div>
							<?php

							}
						?>						
					</div>


					<div class="titlebox" style="border: 1px solid transparent;">	
						<!-- <p>all notifications</p> -->					
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


