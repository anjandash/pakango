<?php
session_start();
include '../functions.php';
$objectsub="";


if(isset($_POST['allobjects'])){
	$objectsub = @$_POST['allobjects'];
}

if($objectsub){
	$num = $_SESSION['objects'];
	$checkdir = true;

	for($j=1; $j<=$num; $j++){
		$_SESSION['dimensions'.$j] = $_POST['dimensions'.$j];
		$_SESSION['weights'.$j] = $_POST['weights'.$j];
		$_SESSION['contents'.$j] = $_POST['contents'.$j];

		/* file upload block */

		if(($_FILES["photo".$j]["tmp_name"]) != ""){			

			date_default_timezone_set('UTC');
			$date = new DateTime();
			$timestamp = $date->getTimestamp();		
			$rand = substr(md5(microtime()),rand(0,26),5);

			$target_dir = "../objectimg/";
			$filename = $rand."-".$timestamp."-".basename($_FILES["photo".$j]["name"]);
			$target_file = $target_dir . $filename;

			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["photo".$j]["tmp_name"]);
			if($check !== false) {
			    echo "<script>console.log('File is an image - " . $check["mime"] . ".');</script>";
			    $uploadOk = 1;
			} else {
			    echo "File entered is not an image. (Object #".$j.")";
			    $uploadOk = 0;
			    $checkdir = false;
			}

			// Check if file already exists
			if (file_exists($target_file)) {
			    echo "Sorry, file already exists. (Object #".$j.")";
			    $uploadOk = 0;
			    $checkdir = false;
			}

			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. (Object #".$j.")";
			    $uploadOk = 0;
			    $checkdir = false;
			}		

			// Check if $uploadOk is set to 0 or set to 1 then upload
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded. (Object #".$j.")";
			    $checkdir = false;
			} else {
			    if (move_uploaded_file($_FILES["photo".$j]["tmp_name"], $target_file)) {
			        echo "<script>console.log('The file ". basename($_FILES["photo".$j]["name"]). " has been uploaded.');</script>";
			        $_SESSION['photos'.$j] = $filename;
			    } else {
			        echo "Sorry, there was an error uploading your file. (Object #".$j.")";
			        $checkdir = false;
			    }
			}
		}

		/* file upload block */
	} //end of for

	if($checkdir){
		header('location: pricing.php');
	} else {
		echo "Something went wrong with the upload(s). Please try again!";
	}	

}



?>


<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%;">

					<div style="height: 100%; overflow-y: scroll;">
					<form action="" method="POST" enctype="multipart/form-data">
					<?php
						$objects = $_SESSION['objects'];

						for ($i=1; $i<=$objects; $i++) {			
							?>
							<div id="objectbox">
							 	<?php echo "<div id='obj' style='margin-bottom: 15px;'>Object <span style='font-family:arial;'>#</span>".$i."</div>"; ?>							 	
								<p id="label">Max. Dimensions:</p>
								<select name="dimensions<?php echo $i; ?>" required>
								  <option value="30cm x 30cm">30cm x 30cm (small)</option>
								  <option value="60cm x 60cm">60cm x 60cm (medium)</option>
								  <option value="120cm x 120cm">120cm x 120cm (large)</option>
								</select><br/><br/>


								<p id="label">Max. Weight:</p>
								<select name="weights<?php echo $i; ?>" required>
								  <option value="Upto 5kg">Upto 5 kg</option>
								  <option value="Upto 8kg">Upto 8kg</option>
								  <option value="Upto 12kg">Upto 12kg</option>
								</select><br/><br/>						 		

								<p id="label">Contents:</p>
						 		<input type="text" name="contents<?php echo $i; ?>" placeholder="Contents, Comments (optional)">



						 		<hr style="border: 1px solid white; margin: 0px auto; width: 10%;">
						 		<input type="file" name="photo<?php echo $i; ?>" style="margin: 10px auto; width: 180px; font-family: arial; border: 1px solid lightgrey;">						 	

							 	<br/><br/>
							</div>
							<hr style="border: 1px solid whitesmoke; margin: 0px auto; width: 82%;">
							<?php											
						}
					?>
					<input type="submit" name="allobjects">
					</form>
					</div>

				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>					