<?php
session_start();
include '../../functions.php';

if(isset($_GET['purposeid'])){
	$purposeid = $_GET['purposeid'];
}

$res = searchTransporterAdsById($purposeid);
$row = mysqli_fetch_assoc($res);

?>

<?php include '../commons/regheader.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<div id="showcard" style="font-size: 14px;">
						<p><b>From:	<?php echo $row['from_city']." (".$row['from_zone'].") <br/>To: ".$row['to_city']." (".$row['to_zone'].")"; ?></b></p>
						<p>Date: <?php echo $row['date']; ?><br/>
						<p>Departure hour: <?php echo $row['pickup_hour']; ?></p>
						<p>Arrival hour: <?php echo $row['delivery_hour']; ?></p>
						<p id="prc" style="margin-top: -35px;"><b>Price: from €<?php echo $row['price_small']; ?></b></p>
						<p id="prcx" style="margin-top: -10px;"><b><?php echo $row['transtype']; ?></b></p>
						By <span style="color: royalblue; font-weight: bolder;"><?php echo $row['transporter']; ?></span></p>
					</div>

					<div id="paycard" style="font-size: 14px;">
						<p style="text-align: center;">Professional services require you to make your payment while booking</p>
						<button class="basebutton" onclick="window.location.href='payx2.php?purposeid=<?php echo $row['id']; ?>'">CONTINUE</button>
					</div>



					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include '../commons/footer.php'; ?>					