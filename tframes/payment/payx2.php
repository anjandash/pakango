<?php
session_start();
include '../../functions.php';

if(isset($_GET['purposeid'])){
	$purposeid = $_GET['purposeid'];
}

$res = searchTransporterAdsById($purposeid);
$row = mysqli_fetch_assoc($res);
?>

<?php
require 'stripe/Stripe.php';

$params = array(
	"testmode"   => "on",
	"private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
	"public_live_key"  => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
	"private_test_key" => "sk_test_fnqdlLjptDUZfKYkDmDqsSsF",
	"public_test_key"  => "pk_test_zsX5MkJQOSp5wy7WvO7Tulgq"
);

if ($params['testmode'] == "on") {
	Stripe::setApiKey($params['private_test_key']);
	$pubkey = $params['public_test_key'];
} else {
	Stripe::setApiKey($params['private_live_key']);
	$pubkey = $params['public_live_key'];
}

if(isset($_POST['stripeToken']))
{
	$amount_cents = str_replace(".","","10.52");  // Chargeble amount
	$invoiceid = "14526321";                      // Invoice ID
	$description = "Invoice #" . $invoiceid . " - " . $invoiceid;
	
	try {

		$charge = Stripe_Charge::create(array(		 
			  "amount" => $amount_cents,
			  "currency" => "usd",
			  "source" => $_POST['stripeToken'],
			  "description" => $description)			  
		);


		if ($charge->card['address_zip_check'] == "fail") {
			throw new Exception("zip_check_invalid");
		} else if ($charge->card['address_line1_check'] == "fail") {
			throw new Exception("address_check_invalid");
		} else if ($charge->card['cvc_check'] == "fail") {
			throw new Exception("cvc_check_invalid");
		}

		// Payment has succeeded, no exceptions were thrown or otherwise caught				

		$result = "success";

	} catch(Stripe_CardError $e) {			

	$error = $e->getMessage();
		$result = "declined";

	} catch (Stripe_InvalidRequestError $e) {
		$result = "declined";		  
	} catch (Stripe_AuthenticationError $e) {
		$result = "declined";
	} catch (Stripe_ApiConnectionError $e) {
		$result = "declined";
	} catch (Stripe_Error $e) {
		$result = "declined";
	} catch (Exception $e) {

		if ($e->getMessage() == "zip_check_invalid") {
			$result = "declined";
		} else if ($e->getMessage() == "address_check_invalid") {
			$result = "declined";
		} else if ($e->getMessage() == "cvc_check_invalid") {
			$result = "declined";
		} else {
			$result = "declined";
		}		  
	}

	
	$cjson = $charge->__toJSON();	
	$store = json_decode($cjson);
	echo "<div style='font-family: arial;'>";
	print_r($cjson);
	echo "</div>";

	// charge details

	$charge_id = $store->id;
	$balance_txn = $store->balance_transaction;

	$charge_currency = $store->currency;
	$charge_amount = $store->amount;
	$timestamp = $store->created;

	$charge_desc = $store->description;
	$failure_code = $store->failure_code;
	$failure_message = $store->failure_message;
	$seller_message = $store->outcome->seller_message;

	$paid = $store->paid;
	$receipt_email = $store->receipt_email;
	$receipt_number = $store->receipt_number;
	$charge_status = $store->status;

	// card details

	$card_id = $store->source->id;
	$card_brand = $store->source->brand;
	$card_country = $store->source->country;
	$card_cvc_check = $store->source->cvc_check;
	$card_exp_month = $store->source->exp_month;
	$card_exp_year = $store->source->exp_year;
	$card_fingerprint = $store->source->fingerprint;
	$card_last4 = $store->source->last4;
	$name_card = $store->source->name;


	if($result == "success"){
		if($charge_status == "succeeded"){

			$chargeq = insertIntoCharges($charge_id, $balance_txn, $charge_currency, $charge_amount, $timestamp, $charge_desc, $failure_code, $failure_message, $seller_message, $paid, $receipt_email, $receipt_number, $charge_status, $card_brand, $card_country, $card_exp_month, $card_exp_year, $card_fingerprint, $card_last4, $name_card);

			if($chargeq){
				$_SESSION['payup'] = $charge_id;
				header('location: payx3.php?purposeid='.$purposeid);
			} else {
				echo "<script>console.log('Error: Insert into Charges Failed!');</script>";
			}

		}
	} elseif ($result == "declined") {
		echo "<script>alert('Sorry! The payment was declined. Try again.');</script>";
	} else {
		echo "<script>alert('Oops! Something went wrong w/ payment. Please contact us.');</script>";
	}
}
?>

<?php include '../commons/regheader.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<div class="dropin-page" style="font-family: arial; border: 1px solid red; padding: 20px; text-align: left;">
						<form action="" method="POST" id="payment-form">
						  <span class="payment-errors"></span>

						  <span style="font-family: arial; font-size: 12px;">Card Number</span>
						  <div class="form-row" style="margin-bottom: 5px;">
						    <label>						      
						      <input type="text" size="20" style="width:285px; height: 30px;" data-stripe="number">
						    </label>
						  </div>

						  
						  <span style="font-family: arial; font-size: 12px;">Expiration (MM / YY)</span>
						  <div class="form-row" style="margin-bottom: 5px;">
						    <label>						      
						      <input type="text" size="2" style="width:80px; height: 30px;" data-stripe="exp_month">
						    </label>
						    <span style="font-family: arial; "> / </span>
						    <input type="text" size="2" style="width:80px; height: 30px;" data-stripe="exp_year">

						    <span style="font-family: arial; font-size: 12px;">CVC</span>
						    <label>								    				      
						      <input type="text" size="4" style="width:80px; height: 30px;" data-stripe="cvc">
						    </label>
						  </div>

						  
						  <div class="form-row" style="margin-bottom: 5px;">
						    
						  </div>

						  <input type="submit" class="submit" style="margin: 30px 40px 10px;" value="Submit Payment">
						</form>
					</div>



					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<!-- TO DO : Place below JS code in js file and include that JS file -->
<script type="text/javascript">
	Stripe.setPublishableKey('<?php echo $params['public_test_key']; ?>');
  
	$(function() {
	  var $form = $('#payment-form');
	  $form.submit(function(event) {
		// Disable the submit button to prevent repeated clicks:
		$form.find('.submit').prop('disabled', true);
	
		// Request a token from Stripe:
		Stripe.card.createToken($form, stripeResponseHandler);
	
		// Prevent the form from being submitted:
		return false;
	  });
	});

	function stripeResponseHandler(status, response) {
	  // Grab the form:
	  var $form = $('#payment-form');
	
	  if (response.error) { // Problem!
	
		// Show the errors on the form:
		$form.find('.payment-errors').text(response.error.message);
		$form.find('.submit').prop('disabled', false); // Re-enable submission
	
	  } else { // Token was created!
	
		// Get the token ID:
		var token = response.id;

		// Insert the token ID into the form so it gets submitted to the server:
		$form.append($('<input type="hidden" name="stripeToken">').val(token));
	
		// Submit the form:
		$form.get(0).submit();
	  }
	};
</script>				

<?php include '../commons/footer.php'; ?>					