<?php
session_start();
include '../../functions.php';

if(isset($_GET['purposeid'])){
	$purposeid = $_GET['purposeid'];
}

if(isset($_SESSION['email_login'])){
	$from_email = $_SESSION['email_login'];
	$resx = getUserDataByEmail($from_email);
	$rowx = mysqli_fetch_assoc($resx);
	$active_sender = $rowx['username'];
}

$res = searchTransporterAdsById($purposeid);
$row = mysqli_fetch_assoc($res);


if(isset($_SESSION['payup'])){

	$from_city = $row['from_city'];
	$to_city = $row['to_city'];
	$adid = $row['id'];
	$sender = $active_sender;
	$transporter = $row['transporter'];
	$status = "requested";


	// ------------------------------------------

	$xquery = chatcheck($sender, $transporter, $purposeid);
	$xrow = mysqli_fetch_assoc($xquery);

	if($xrow == 0){} else { $getobjectcode = $xrow['objectcode']; }

	// ------------------------------------------	

	unset($_SESSION['payup']);
	$insquery = insertSenderRequest($from_city, $to_city, $adid, $sender, $transporter, $getobjectcode, $status);


	if($insquery){

		$date = new DateTime();
		$timestamp = $date->getTimestamp();

		$notiftype = "booking";
		$notifqueryx = insertNotifs($sender, $transporter, $timestamp, $adid, $notiftype);
		if($notifqueryx){}else{echo "<script>console.log('Error: Insert into Notifs failed! (booking)');</script>";}


	}else{ 
		echo "Error: Sorry! your booking could not be processed!"; 
	}

}

?>

<?php include '../commons/regheader.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<div id="sux">
						Payment Success!
					</div>

					<div id="showcard"  style="font-size: 14px;">
						<p><b>From:	<?php echo $row['from_city']." (".$row['from_zone'].") <br/>To: ".$row['to_city']." (".$row['to_zone'].")"; ?></b></p>
						<p>Date: <?php echo $row['date']; ?><br/>
						<p>Departure hour: <?php echo $row['pickup_hour']; ?></p>
						<p>Arrival hour: <?php echo $row['delivery_hour']; ?></p>
						<p id="prc" style="margin-top: -35px;"><b>Price: from €<?php echo $row['price_small']; ?></b></p>
						<p id="prcx" style="margin-top: -10px;"><b><?php echo $row['transtype']; ?></b></p>
						By <span style="color: royalblue; font-weight: bolder;">@<?php echo $row['transporter']; ?></span></p>
					</div>

					<div id="paycard"  style="font-size: 14px;">
					<p>We have received your payment!</p>
					<p>Your Booking request has been sent to <span style="color: royalblue;"><b>@<?php echo $row['transporter']; ?></b></span></p>
					<button class="basebutton" onclick="window.location.href='../status.php?adid=<?php echo $adid; ?>'">CONTINUE</button>
					</div>



					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include '../commons/footer.php'; ?>					
