<?php
session_start();
include ('../functions.php');


// some added php code block
if(isset($_SESSION['activetype'])){
if(($_SESSION['activetype'] == "sender")){

	if(isset($_GET['ad'])){
		$adid=$_GET['ad'];
		$_SESSION['transadactive'] = $adid; 
	}
}
}

if(isset($_SESSION['transadactive'])){
	$id = $_SESSION['transadactive'];
	$res1 = searchTransporterAdsById($id);
	$row = mysqli_fetch_assoc($res1);
}else{
	header('location: profile.php');
	exit();
}

?>

<?php include 'commons/header.php'; ?>

				<!-- base code for the web app-->
				<div id="frame1" style="background-color: white;">

					<p id="topcard">Space, Weight and Transport Information</p>

								
						<div style="width: 70%; height: auto; text-align: left; margin: 10px auto; font-family: calibri;">				
							<span style="color: royalblue; font-weight: bolder;"><?php echo $row['transporter']; ?></span><br/>
							<span style="font-size: 14px; font-family: calibri; font-style: italic;">Date: <?php echo $row['date']; ?></span><br/><br/>
							<span style="font-weight: bold;">
							<?php echo $row['pickup_hour']; ?> - <?php echo $row['from_zone']; ?><br/>
							</span>
							<span style="color: lightgrey;">
							<?php echo $row['from_city'] ?><br/>
							</span>
							
							<span style="font-weight: bold;">
							<?php echo $row['delivery_hour']; ?> - <?php echo $row['to_zone']; ?><br/>
							</span>
							<span style="color: lightgrey;">
							<?php echo $row['to_city'] ?><br/>	
							</span>	
								
							<br/>
							<p style="font-size: 14px;">Space Available:</p>
							<span style="color: lightgrey; font-size: 14px;"> 
								(Upto: <?php echo $row['max_dimensions']; ?>)
							</span>	
							<p style="font-size: 14px;">Maximum Weight:</p>			
							<span style="color: lightgrey; font-size: 14px;"> 
								(Upto: <?php echo $row['max_weight']; ?>)
							</span>	
							
							<br/><br/>	
							<span style="font-family: arial;">Price: €<?php echo $row['price_small']; ?> / €<?php echo $row['price_medium']; ?> / €<?php echo $row['price_large']; ?></span><br/>
							<span style="font-size: 12px;">This is a <span style="color: orangered;"><?php echo $row['transtype']; ?></span> option.</span>
								

						</div>				
								
			
	<button class="subxbutton" style='background: #46DBDC;' onclick="window.location.href='chat.php?to=<?php echo $row['transporter']; ?>&purposeid=<?php echo $adid; ?>'">CHAT</button>
	<div style="height: 10px; width: 80%; border-bottom: 1px solid lightgrey; margin: -10px auto 10px;">
		<p style="width: 22px; background: white; margin: 0px auto; color: lightgrey;">or</p>
	</div>
	<button class="subxbutton" style="margin-top:10px;" onclick="window.location.href='chat.php?to=<?php echo $row['transporter']; ?>&purposeid=<?php echo $adid; ?>&book=active'">BOOK</button>
						
				</div>

<?php include 'commons/footer.php'; ?>			
