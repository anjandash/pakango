<?php
session_start();
include('../functions.php');
$pricesub="";

if(isset($_POST['pricesub'])){
	$pricesub = @$_POST['pricesub'];
}

if($pricesub){
	$_SESSION['price'] = "dummprice";

	$_SESSION['price-small'] = @$_POST['transprice-small'];
	$_SESSION['price-medium'] = @$_POST['transprice-medium'];
	$_SESSION['price-large'] = @$_POST['transprice-large'];

	header("location: summary.php");
	exit();
}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<form id="price-form" action="" method="POST">
						<div style="width: 70%; margin: 0px auto; color: grey;">Prezzo consigliato per il trasporto di un pacco media su questa tratta:</div>
						<br/>	

						<div id="prbox">	
							<p id="prlist">Small Package:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;						
								<input type="number" id="transprice" name="transprice-small" min="0" max="100" value="5" required><br/>
							</p><br/>

							<p id="prlist">Medium Package:&nbsp;&nbsp;
								<input type="number" id="transprice" name="transprice-medium" min="0" max="100" value="10" required><br/>
							</p><br/>
							
							<p id="prlist">Large Package:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" id="transprice" name="transprice-large" min="0" max="100" value="15" required><br/>
							</p>
						</div>

						<br/>
						<div style="width: 70%; margin: 0px auto; color: grey;">Modifica o pubblica con prezzo consigliato</div>		
						<br/>

						<input type="submit" name="pricesub" value="CONTINUE"><br/>
						<!-- <div class="backbutton" onclick="window.location.href='dimensions.php'">
							<p>BACK</p>
						</div> -->						
					</form>

					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>


