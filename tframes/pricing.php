<?php
session_start();
include ('../functions.php');
$active_username = "";

if(isset($_SESSION['email_login'])){
	$email = $_SESSION['email_login'];
	$reschk = getUserDataByEmail($email);
	$rowchk = mysqli_fetch_assoc($reschk);
	$active_username = $rowchk['username'];
}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<script type="text/javascript">
						function clk(str) {
						  var xhttp = new XMLHttpRequest();
						  xhttp.onreadystatechange = function() {
						    if (this.readyState == 4 && this.status == 200) {
						    	var response = JSON.parse(this.responseText);
						    	console.log(response);

						    	

								if(response == null){
									alert('Sorry! No results match!');
								} else {

									var list = document.getElementById('resp');
									list.innerHTML = "";

									if(response[0]['id'] == null){
										list.innerHTML = "<div style='margin: 50px auto; color: grey;'>Sorry! No results match!</div>"
									} else {
										for(var i=0; i < response.length; i++){								

											var div = document.createElement("div");
											div.setAttribute('class', 'resads');
											div.setAttribute('onclick', 'handle('+ response[i]['id'] +')');	

											var div2 = document.createElement("div");
											var div3 = document.createElement("div");
											div2.setAttribute('id', 'prc_pr');
											div2.setAttribute('style', 'margin-top: -65px; margin-right: 8px;');
											div3.setAttribute('id', 'prcx_pr');
											div3.setAttribute('style', 'margin-top: -32px; margin-right: 8px;')

											div.innerHTML = "<div id='maincard'>" + 
											"<div id='carddp'></div>" +
											"<span style='font-size:18px;'><b>" + response[i]['transporter'] + "</b></span><br/>" +
											"<span style='font-size:12px;'>" + response[i]['date'] + 
											"</div>" +
											"<div style='padding: 5px 12px;'>" +
											"<b>" + response[i]['pickup_hour'] + " " + response[i]['from_zone'] + "</b><br/>" + 
											"<span style='color: lightgrey;'>" + response[i]['from_city'] + "</span><br/>" +
											"<b>" + response[i]['delivery_hour'] + " " + response[i]['to_zone'] + "</b><br/>" +
											"<span style='color: lightgrey;'>" + response[i]['to_city'] + "</span><br/>" +
											"</div>";

											div2.innerHTML = "€" + response[i]['price_small'] + " <span style='font-family:arial;'>/</span> €" + response[i]['price_medium'] + " <span style='font-family:arial;'>/</span> €" + response[i]['price_large'];
											div3.innerHTML = response[i]['transtype'];
											div.appendChild(div2);
											div.appendChild(div3);

											list.appendChild(div);											
									    }										

									}
									
								}
						    }
						  };
						  xhttp.open("GET", "loadfilter.php?filter=" + str + "&t=" + Math.random(), true);
						  xhttp.send();
						}

						function handle(setid){
							window.location.href = "prechat.php?ad="+ setid;
							
						}
					</script>

					<div style="width: auto; height: 40px; background: whitesmoke; border: 1px solid whitesmoke; box-sizing: border-box; padding: 5px 10px;">
						<div class="checkfil"><input type="radio" id="radioll" name="filter" value="all" onclick="clk(this.value)"> All</div> 
						<div class="checkfil"><input type="radio" name="filter" value="priv" onclick="clk(this.value)"> Only Private</div>
						<div class="checkfil"><input type="radio" name="filter" value="pro" onclick="clk(this.value)"> Only Professional</div>
					</div>

					<br/>
					<div id="notifbar" style="background: whitesmoke;">
						<!-- php loop to load all matches from db -->
													
							<div id="resp" style="font-family: arial;">
							</div>
														

						<div style="margin: 20px auto;">
						<a href="../index.php" style="font-size: 14px;">Do a new search</a>
						</div>			
					</div>

				</div>

				<script type="text/javascript">
					window.onload = onPageLoad();

					function onPageLoad() {
					  document.getElementById("radioll").click();
					}
				</script>

<?php include 'commons/footer.php'; ?>