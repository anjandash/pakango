<?php

session_start();
include ('../functions.php');

if(!isset($_SESSION["email_login"])){
	header('location: login.php');
	exit();
}

$email = $_SESSION['email_login'];
$res = getUserDataByEmail($email);
$row = mysqli_fetch_assoc($res);
$username = $row['username'];
$adder = "default";

if(isset($_GET['m'])){
	$message = $_GET['m'];
	if($message == "sameuser"){
		echo "<div class='messenger' style='opacity: 1; padding: 5px;'>Sorry! You cannot book your own ad!</div>";
	}	
} else {
	$message = "";
}

//TRANSPORTER

if(isset($_SESSION['activetype'])){
	if($_SESSION['activetype'] == "transporter"){

		// STORE THE FLOW ORDER FROM SESSION VARIABLES
		if(isset($_SESSION['from_city'])){

		$from_city = $_SESSION['from_city'];
		$to_city = $_SESSION['to_city'];

		$from_zone = $_SESSION['from_zone'];
		$to_zone = $_SESSION['to_zone'];
		$pickup_hour = $_SESSION['pickup_hour'];
		$delivery_hour = $_SESSION['delivery_hour'];

		$date = $_SESSION['date'];
		$size = $_SESSION['size'];
		
		$max_dimensions = $_SESSION['max_dimensions'];
		$max_weight = $_SESSION['max_weight'];
		$contents = $_SESSION['contents'];

		$price = $_SESSION['price'];

		$price_small = $_SESSION['price-small'];
		$price_medium = $_SESSION['price-medium'];
		$price_large = $_SESSION['price-large'];

		$transtype = $row['transtype'];

		$adder = insertTransporterAds($from_city, $to_city, $date, $from_zone, $to_zone, $pickup_hour, $delivery_hour, $size, $max_dimensions, $max_weight, $contents, $price, $price_small, $price_medium, $price_large, $username, $transtype);
		}

		// UNSET ALL SESSION VARIABLES

		if($adder){	
			unset($_SESSION['from_city']);
			unset($_SESSION['to_city']);

			unset($_SESSION['from_zone']);	
			unset($_SESSION['to_zone']);
			unset($_SESSION['pickup_hour']);
			unset($_SESSION['delivery_hour']);

			unset($_SESSION['date']);
			unset($_SESSION['size']);
			unset($_SESSION['max_dimensions']);
			unset($_SESSION['max_weight']);
			unset($_SESSION['contents']);
			unset($_SESSION['price']);
			unset($_SESSION['price-small']);
			unset($_SESSION['price-medium']);
			unset($_SESSION['price-large']);
			unset($_SESSION['activetype']);	
			echo "<div class='messenger' style='opacity: 1; padding: 5px;'>Your ad has been published!</div>";
		} else{
			printf("Error: The ad could not be placed! %s\n\n", mysqli_error($con));
			echo "<div class='messenger' style='opacity: 1; padding: 5px;'>Oops! Error: Your ad could not be placed!</div>";
		}
	}
}

// ##############################

if(isset($_SESSION['transadactive'])){
	$id = $_SESSION['transadactive'];
	$res1 = searchTransporterAdsById($id);
	$row = mysqli_fetch_assoc($res1);
	$transporter = $row['transporter'];

	header('location: chat.php?to='.$transporter.'&purposeid='.$id.'');
	exit();
}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<div id="pcard">
						<div id="usercard">	
							Welcome!	
							<div style="font-weight: bolder; padding-bottom: 5px;">@<?php echo $username; ?></div>
							<!-- <div id="logoutbut" onclick="window.location.href='../logout.php'">logout</div> -->					
						</div>

						<div id="notifbutton" onclick="window.location.href='ads.php'">
						my ads
						</div><br/>

						<div id="notifbutton" onclick="window.location.href='chatlist.php'">
						chats
						</div>						
					</div>

					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>
