<?php
session_start();

if(isset($_POST['reg'])){

	$_SESSION['fullname'] = @$_POST['fullname'];
	$_SESSION['dob'] = @$_POST['dob'];
	$_SESSION['username'] = @$_POST['username'];
	$_SESSION['email'] = @$_POST['email'];
	
	$pswd = @$_POST['password'];
	$_SESSION['passwordtxt'] = $pswd;
	$pswd = openssl_digest($pswd, 'sha512');
	$_SESSION['password'] = $pswd;

	if($_SESSION['transtype'] == 'private'){
		header('location: regx2.php');		
	} else if($_SESSION['transtype'] == 'professional') {
		header('location: regxpro.php');		
	}

}

?>

<?php include '../commons/regheader.php'; ?>

<script type="text/javascript">

function checkUsername(str) {
    if (str.length == 0) { 
        document.getElementById("uinput_img").src = "../../images/icons/dormant.png";
        document.getElementById("hintText").innerHTML = "";
        document.getElementById("logsub").style.opacity = "0.5";        
        document.getElementById("logsub").disabled = true;
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

            	if(this.responseText == "OK"){
            		document.getElementById("uinput_img").src = "../../images/icons/success.png";
            		document.getElementById("hintText").innerHTML = "";
            		document.getElementById("logsub").style.opacity = "1";
            		document.getElementById("logsub").disabled = false;
            	}

            	if(this.responseText == "STOP"){
            		document.getElementById("uinput_img").src = "../../images/icons/error.png";
            		document.getElementById("hintText").innerHTML = "Username already taken!"
            		document.getElementById("logsub").style.opacity = "0.5";
            		document.getElementById("logsub").disabled = true;
            	} 

            	if(this.responseText == "ERROR"){
            		document.getElementById("uinput_img").src = "../../images/icons/dormant.png";
            		document.getElementById("hintText").innerHTML = "This username is too common. Please try another.";            		
            		document.getElementById("logsub").style.opacity = "0.5";            		
            		document.getElementById("logsub").disabled = true;
            	}            	
                
            }
        };
        xmlhttp.open("GET", "checkusername.php?q=" + str, true);
        xmlhttp.send();
    }
}


function checkEmail(str) {

    if (str.length == 0) { 
        document.getElementById("einput_img").src = "../../images/icons/dormant.png";
        document.getElementById("logsub").style.opacity = "0.5";        
        document.getElementById("logsub").disabled = true;
        emailConf();
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

            	if(this.responseText == "OK"){
            		document.getElementById("einput_img").src = "../../images/icons/success.png";
            		document.getElementById("hintText2").innerHTML = "";
            		document.getElementById("logsub").style.opacity = "1";
            		document.getElementById("logsub").disabled = false;
                    emailConf();
            	}

            	if(this.responseText == "STOP"){
            		document.getElementById("einput_img").src = "../../images/icons/error.png";
            		document.getElementById("hintText2").innerHTML = "Sorry! An sccount with this email already exists.";
            		document.getElementById("logsub").style.opacity = "0.5";
            		document.getElementById("logsub").disabled = true;
                    emailConf();
            	} 

            	if(this.responseText == "ERROR"){
            		document.getElementById("einput_img").src = "../../images/icons/dormant.png";
            		document.getElementById("hintText2").innerHTML = "This email is savage. Please try another.";
            		document.getElementById("logsub").style.opacity = "0.5";            		
            		document.getElementById("logsub").disabled = true;
                    emailConf();
            	}            	
                
            }
        };
        xmlhttp.open("GET", "checkemail.php?q=" + str, true);
        xmlhttp.send();
    }
}	


function emailConf(){
    var $email1 = document.getElementById('einput').value;
    var $email2 = document.getElementById('cinput').value;

    if($email2 == ""){
        document.getElementById("cinput_img").src = "../../images/icons/dormant.png";
        document.getElementById("logsub").style.opacity = "0.5";        
        document.getElementById("logsub").disabled = true;
    } else {

        if ($email1 == $email2){
            document.getElementById("cinput_img").src = "../../images/icons/success.png";
            document.getElementById("hintText3").innerHTML = "";
            document.getElementById("logsub").style.opacity = "1";
            document.getElementById("logsub").disabled = false;     
        }else{
            document.getElementById("cinput_img").src = "../../images/icons/error.png";
            document.getElementById("hintText3").innerHTML = "Please retype your emails to match.";
            document.getElementById("logsub").style.opacity = "0.5";
            document.getElementById("logsub").disabled = true;      
        }
    }


 }

</script>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%;">

					<div style="height: 100%; overflow-y: scroll; overflow-x: hidden;">
					<form id="register-form" action="" method="POST" autocomplete="off">
						<div style="margin: 5px auto;"></div>
							
							<input type="hidden" name="email" placeholder="Email" autocomplete="off">
							<input type="hidden" name="password" placeholder="Password" autocomplete="off">
							
							<input type="text" name="fullname" placeholder="Fullname" autocomplete="off" required>
							<input type="date" name="dob" placeholder="Date of Birth" autocomplete="off" required>

					<div id="uinput_container">
					   <input type="text" id="uinput" name="username" placeholder="Username" autocomplete="off" onkeyup="checkUsername(this.value)" required>
					   <img src="../../images/icons/dormant.png" id="uinput_img">
					</div>	

					<div id="einput_container">
					  <input type="email" id="einput" name="email" placeholder="Email" autocomplete="new-password" onkeyup="checkEmail(this.value)" required>
					  <img src="../../images/icons/dormant.png" id="einput_img">
					</div>

                    <div id="cinput_container">
                      <input type="email" id="cinput" name="confemail" placeholder="Confirm Email" autocomplete="new-password" onkeyup="emailConf()" required>
                      <img src="../../images/icons/dormant.png" id="cinput_img">
                    </div>                    


							
							<input type="password" name="password" placeholder="Password" autocomplete="new-password" required>

							<?php
								// PASSWORD STRENGTH VERIFICATION
								// CHECK IF EMAILS MATCH
								// CHECK IF USERNAME EXISTS USING AJAX
							?>


							<!-- <div id="terms">
								<input type="checkbox" name="terms" onclick="return false" checked>
								<label> I agree to Pakango <a href="" target="blank">Terms</a></label>
							</div> -->


							<div id="hintText"></div>
							<div id="hintText2"></div>
                            <div id="hintText3"></div>


							<input type="submit" id="logsub" name="reg" value="continue"><br/>
							<div class="titlebox" id="reglink" onclick="window.location.href='login.php'">login page</div>
					</form>		
					</div>				

				</div>
				<!-- base code for the web app-->

<?php include '../commons/footer.php'; ?>