<?php
session_start();
include ('../functions.php');

if(!isset($_SESSION['email_login'])){
	header('location: login.php?m=loginfirst');
	exit();
}

if(isset($_GET['adid'])){
	$adid = $_GET['adid'];


	if(isset($_GET['senderx'])){
		$act_email = $_SESSION['email_login'];
		$resv = getUserDataByEmail($act_email);
		$rowv = mysqli_fetch_assoc($resv);
		$activeuser = $rowv['username'];

		$sender = $_GET['senderx'];

	} else {
		$sender_email = $_SESSION['email_login'];
		$resx = getUserDataByEmail($sender_email);
		$rowx = mysqli_fetch_assoc($resx);
		$activeuser = $rowx['username'];

		$sender = $rowx['username'];
	}
}
?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1" style="height: 85%;">

					<div style="height: 100%; overflow-y: scroll;">
					<div>
						<?php

						$res = searchTransporterAdsById($adid);
						$row = mysqli_fetch_assoc($res);

						$findquery = getRequestStatus($adid, $sender);
						while($rowz = mysqli_fetch_assoc($findquery)){
						?>
							<div style="width: 70%; height: auto; text-align: left; margin: 10px auto; font-family: calibri;">				
								<span style="color: royalblue; font-weight: bolder;"><?php echo $row['transporter']; ?></span><br/>
								<span style="font-size: 14px; font-family: calibri; font-style: italic;">Date: <?php echo $row['date']; ?></span><br/><br/>
								<span style="font-weight: bold;">
								<?php echo $row['pickup_hour']; ?> - <?php echo $row['from_zone']; ?><br/>
								</span>
								<span style="color: lightgrey;">
								<?php echo $row['from_city'] ?><br/>
								</span>
								
								<span style="font-weight: bold;">
								<?php echo $row['delivery_hour']; ?> - <?php echo $row['to_zone']; ?><br/>
								</span>
								<span style="color: lightgrey;">
								<?php echo $row['to_city'] ?><br/>	
								</span>	
								<hr style='border:1px solid whitesmoke; width: 75%; margin: 10px 0px;'>
								<div id="prc_pr" style="margin-top: -1px; padding: 4px 5px 3px; line-height: 15px;">Price: From €<?php echo $row['price_small'] ?></div>
								<div id="prcx_pr" style="margin-top: -1px;"><?php echo $row['transtype'] ?></div>

							</div>

							<br/>
							<br/>

							<div style="font-family: calibri; font-size: 14px; color: grey;">
							<?php


							
							if($rowz['status'] == "requested"){
								echo "<div class='statusbox requested'>requested</div>";								

								if($activeuser == $rowz['transporter']){
									echo "You have not accepted this request yet";
								} else {
									echo $rowz['transporter']." has not accepted the request yet!";
									echo "<br/><br/><br/><br/><a href='profile.php' style='text-decoration: none; cursor:pointer; color: grey; padding: 8px 12px; margin: 15px 0px; border: 1px solid grey; border-radius: 3px;'>Back to Profile</a>";
								}
							}

							if($rowz['status'] == "accepted"){
								echo "<div class='statusbox accepted'>accepted</div>";

								if($activeuser == $rowz['transporter']){
									echo "You have accepted this request";
									echo "<br/><br/><br/><br/><a href='profile.php' style='text-decoration: none; cursor:pointer; color: grey; padding: 8px 12px; margin: 15px 0px; border: 1px solid grey; border-radius: 3px;'>Back to Profile</a>";
								} else {
									echo $rowz['transporter']." has <span style='color: green;'>accepted</span> your request!";
									echo "<br/><hr style='border:1px solid whitesmoke; width: 80%; margin: 10px auto;'><p>Generated QR code for booking here!</p>";
									?>
									<img src="../qrcodes/<?php echo $rowz['qrcode']; ?>" style="height: 200px; width: 200px; border: 1px solid white;"><br/>
									<p>TRANS CODE: <span style="font-family: arial; font-weight: bolder;"><?php echo $rowz['numbercode']; ?></span></p>

									<!-- <div class="sharer" onclick="">Send code to receiver</div> -->

									<?php


								}
								
							}

							if($rowz['status'] == "declined"){
								echo "<div class='statusbox declined'>declined</div>";

								if($activeuser == $rowz['transporter']){
									echo "You have declined this request";
								} else {
									echo $rowz['transporter']." has <span style='color: red;'>declined</span> your request!";
								}
							}
							?>
							</div>

						<?php														
						}
						?>
							
					</div>
					</div>


					<div class="titlebox" style=" width: 100px; height: 0px; line-height: 20px; color: grey; border: 0px solid grey; font-size: 14px; padding: 5px; margin: 0px auto 0px;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>
