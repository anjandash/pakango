<?php
session_start();
include('../functions.php');

// if(isset($_SESSION["email_login"])){
// 	header('location: profile.php');
// 	exit();
// }

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">
					<div id="pcard">					
						Your Ad summary!
						<br/>
						<br/>
						<div style="text-align: left;">
						<?php
						if( isset($_SESSION["from_city"]) && isset($_SESSION["to_city"]) &&
							isset($_SESSION["from_zone"]) && isset($_SESSION["to_zone"]) &&
							isset($_SESSION["pickup_hour"]) && isset($_SESSION["delivery_hour"]) &&
							isset($_SESSION["date"]) && isset($_SESSION["size"]) &&
							isset($_SESSION["price"]) ){

							echo "<div style='font-size: 14px; padding: 0px 10px;'>";

							echo "From: ".$_SESSION['from_city']."<br/>";
							echo "Zone: ".$_SESSION['from_zone']."<br/>";
							echo "Date: ".$_SESSION['date']."<br/>";
							echo "Pickup: ".$_SESSION['pickup_hour']." hrs<br/>";

							echo "<hr>";

							echo "To: ".$_SESSION['to_city']."<br/>";
							echo "Zone: ".$_SESSION['to_zone']."<br/>";
							echo "Date: ".$_SESSION['date']."<br/>";
							echo "Delivery: ".$_SESSION['delivery_hour']." hrs<br/>";

							echo "<hr>";

							echo "Max. Dimensions: ".$_SESSION['max_dimensions']."<br/>";
							echo "Max. Weight: ".$_SESSION['max_weight']."<br/>";
							echo "Price range: €".$_SESSION['price-small']."- €".$_SESSION['price-medium']."- €".$_SESSION['price-large']."<br/></div>";
						}
						else{
							echo "<p style='font-size: 12px;'><i>Sorry! Not all ad fields are entered. <a href='../index.php'>Restart</a></i></p>";
						}
						?>
						</div>
						<br/>
						<br/>
						
						<button class="subxbutton" style="background: linear-gradient(90deg, #FD9B18 0%, #FE6159 100%); color: white;" onclick="window.location.href='login.php'">CONFIRM</button>
						<button class="backxbutton" style="background: transparent;" onclick="window.location.href='../index.php'">REDO</button>
						</div>



					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>

<?php include 'commons/footer.php'; ?>


