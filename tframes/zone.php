<?php
session_start();
include('../functions.php');
$zonesub="";

if(isset($_POST['zonesub'])){
	$zonesub = @$_POST['zonesub'];
}

if($zonesub){
	$_SESSION['from_zone'] = @$_POST['fromzone'];
	$_SESSION['to_zone'] = @$_POST['tozone'];

	header("location: dimensions.php");
	exit();
}

?>

<?php include 'commons/header.php'; ?>
				
				<!-- base code for the web app-->
				<div id="frame1">

					<form id="zone-form" action="" method="POST">
						<p id="label">Zona di ritiro in <?php echo $_SESSION['from_city']; ?>:</p>

						<?php
							$fromcity = $_SESSION['from_city'];
							$pquery = getPickupPoints($fromcity);

							?>
							<select name="fromzone" required>
						  	<option value="">Select Departure Zone</option>
						  	<?php

							while($pres = mysqli_fetch_assoc($pquery)){

								?>
								<option value="<?php echo $pres['zonelist'] ?>"><?php echo $pres['zonelist'] ?></option>
								<?php

							}
						?>
						</select><br/><br/><br/>


						<p id="label">Zona di consegna in <?php echo $_SESSION['to_city']; ?>:</p>
						<?php
							$tocity = $_SESSION['to_city'];
							$prquery = getPickupPoints($tocity);

							?>
							<select name="tozone" required>
						  	<option value="">Select Arrival Zone</option>
						  	<?php

							while($prres = mysqli_fetch_assoc($prquery)){

								?>
								<option value="<?php echo $prres['zonelist'] ?>"><?php echo $prres['zonelist'] ?></option>
								<?php

							}
						?>
						</select><br/><br/>

						<input type="submit" name="zonesub" value="NEXT"><br/>					
					</form>

					<div class="titlebox" style="border: 1px solid transparent;">						
					</div>
				</div>
				<!-- base code for the web app-->

<?php include 'commons/footer.php'; ?>				




